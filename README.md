# Enzymatic Activity Calculations
R scripts to calculate enzymatic activities following enzymatic assay protocols from Linda van Diepen (v. Jul 2020)

Author: Félix Brédoire (contact me on my uwyo email with any question)

*v. 2021-01-06*

## Content of directories:

- `howto`: instructions to generate usable raw files from the Gen5 software controlling the plate reader, fill the metadata templates, and use the series of R scripts

- `test_day`: R scripts and metadata templates for testing substrate concentrations and incubation times on a few samples (typically 3 samples; the example here has 6 samples, i.e. 2 plates per substrate)

- `sample_day`: R scripts and metadata templates after running a batch of samples once optimal substrate concentrations and incubation times have been determined (usually they have been determined on test day)

- `sample_day_kinetics`: same as sample_day but when multiple measurements were performed at different incubation times


