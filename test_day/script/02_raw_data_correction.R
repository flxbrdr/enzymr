# Script description ------------------------------------------------------

## Enzymatic Assays
## Treatment of Test Day data
## Correction of raw data
## 2020-11-12
## Félix Brédoire


# Load R packages ---------------------------------------------------------

library(data.table)


# Load and prepare data ---------------------------------------------------

DT <- fread("data/tmp/all_raw_readings.txt")
DT[, TIME_READ := as.ITime(TIME_READ)]


# Raw stats ---------------------------------------------------------------

## mean, mean-sd, mean+sd per PLATE_ID, WELL_CONTENT, and TARGET_INCUB_TIME
DT[, MEAN := mean(RAW, na.rm = TRUE), by = .(PLATE_DESIGN, PLATE_ID, WELL_CONTENT, TARGET_INCUB_TIME)]
DT[, MEANmSD := mean(RAW, na.rm = TRUE) - sd(RAW, na.rm = TRUE), by = .(PLATE_DESIGN, PLATE_ID, WELL_CONTENT, TARGET_INCUB_TIME)]
DT[, MEANpSD := mean(RAW, na.rm = TRUE) + sd(RAW, na.rm = TRUE), by = .(PLATE_DESIGN, PLATE_ID, WELL_CONTENT, TARGET_INCUB_TIME)]


# Remove outliers ---------------------------------------------------------

## RAW has to be included in ]MEANpSD:MEANmSD[
DT[, RAW2 := ifelse(RAW > MEANpSD | RAW < MEANmSD, NA, RAW)]


# Write output ------------------------------------------------------------

fwrite(DT, "data/tmp/all_raw_data_corrected.txt", na = NA, quote = FALSE)

