# Script description ------------------------------------------------------

## Enzymatic Assays
## Treatment of Test Day data
## Check Blanks: were blanks stable over time?
## 2020-12-08
## Félix Brédoire


# Load R packages ---------------------------------------------------------

library(data.table)
library(ggplot2)


# Load and prepare data ---------------------------------------------------

DT <- fread("data/tmp/all_raw_data_corrected.txt")
DT[, TIME_READ := as.ITime(TIME_READ)]
DT[, TIMESTAMP := as.POSIXct(paste(DATE_READ, TIME_READ), format = "%Y-%m-%d %H:%M:%S")]

dir.create("fig/blanks", recursive = TRUE)


# Black: buffer -----------------------------------------------------------

BB <- DT[grepl("black_substrate", PLATE_DESIGN) & grepl("^buffer(\\d|\\d.\\d+)?$", WELL_CONTENT)]

ggplot(BB[!is.na(RAW2)], aes(x = TIMESTAMP, y = RAW2, color = WELL_ID)) +
  facet_wrap(~ PLATE_ID) +
  geom_point() +
  geom_line() +
  ggtitle("Black Plates - Buffer") +
  xlab("Time of Reading") +
  ylab("Raw Reading (corrected)") +
  theme_bw() +
  theme(
    aspect.ratio = 1
  )

ggsave("fig/blanks/black_buffer_check.png", width = 9, height = 9, units = "in")


# Black: sample+buffer ----------------------------------------------------

BSB <- DT[grepl("black_blank", PLATE_DESIGN) & grepl("sample\\d+\\+buffer(\\d|\\d.\\d+)?$", WELL_CONTENT) & !is.na(WELL_CONTENT)]
BSB[, SAMPLE_ID := gsub("\\+buffer(\\d|\\d.\\d+)?$", "", WELL_CONTENT)]

ggplot(BSB[!is.na(RAW2)], aes(x = TIMESTAMP, y = RAW2, color = WELL_ID)) +
  facet_wrap(~ SAMPLE_ID) +
  geom_point() +
  geom_line() +
  scale_color_viridis_d() +
  ggtitle("Black Plates - Sample+buffer") +
  xlab("Time of Reading") +
  ylab("Raw Reading (corrected)") +
  theme_bw() +
  theme(
    aspect.ratio = 1
  )

ggsave("fig/blanks/black_sample+buffer_check.png", width = 9, height = 6, units = "in")


# Clear: buffer -----------------------------------------------------------

CB <- DT[grepl("clear_substrate", PLATE_DESIGN) & grepl("^buffer(\\d|\\d.\\d+)?$", WELL_CONTENT)]
CB[, PLATE_NB := gsub("\\D+", "", PLATE_ID)]

ggplot(CB[!is.na(RAW2)], aes(x = TIMESTAMP, y = RAW2, color = WELL_ID)) +
  facet_grid(SUBSTRATE ~ PLATE_NB, scales = "free_y") +
  geom_point() +
  geom_line() +
  ggtitle("Clear Plates - Buffer") +
  xlab("Time of Reading") +
  ylab("Raw Reading (corrected)") +
  theme_bw() +
  theme(
    aspect.ratio = 1
  )

ggsave("fig/blanks/clear_buffer_check.png", width = 5, height = 6, units = "in")


# Clear: sample+buffer (ABTS) ---------------------------------------------

CSBA <- DT[SUBSTRATE == "ABTS" & grepl("sample\\d+\\+buffer(\\d|\\d.\\d+)?", WELL_CONTENT)]
CSBA[, SAMPLE_ID := gsub("\\+buffer(\\d|\\d.\\d+)?$", "", WELL_CONTENT)]

ggplot(CSBA[!is.na(RAW2)], aes(x = TIMESTAMP, y = RAW2, color = WELL_ID)) +
  facet_wrap(~ SAMPLE_ID) +
  geom_point() +
  geom_line() +
  scale_color_viridis_d() +
  ggtitle("Clear Plates - Sample+Buffer (ABTS)") +
  xlab("Time of Reading") +
  ylab("Raw Reading (corrected)") +
  theme_bw() +
  theme(
    aspect.ratio = 1
  )

ggsave("fig/blanks/clear_sample+buffer_abts_check.png", width = 9, height = 6, units = "in")


# Clear: sample+buffer (PHENOX) -------------------------------------------

CSBPH <- DT[SUBSTRATE == "PHENOX" & grepl("sample\\d+\\+buffer(\\d|\\d.\\d+)?", WELL_CONTENT)]
CSBPH[, SAMPLE_ID := gsub("\\+buffer(\\d|\\d.\\d+)?$", "", WELL_CONTENT)]

ggplot(CSBPH[!is.na(RAW2)], aes(x = TIMESTAMP, y = RAW2, color = WELL_ID)) +
  facet_wrap(~ SAMPLE_ID) +
  geom_point() +
  geom_line() +
  scale_color_viridis_d() +
  ggtitle("Clear Plates - Sample+Buffer (PHENOX)") +
  xlab("Time of Reading") +
  ylab("Raw Reading (corrected)") +
  theme_bw() +
  theme(
    aspect.ratio = 1
  )

ggsave("fig/blanks/clear_sample+buffer_phenox_check.png", width = 9, height = 6, units = "in")


# Clear: sample+buffer (PEROX) --------------------------------------------

CSBPE <- DT[SUBSTRATE == "PEROX" & grepl("sample\\d+\\+buffer(\\d|\\d.\\d+)?", WELL_CONTENT)]
CSBPE[, SAMPLE_ID := gsub("\\+buffer(\\d|\\d.\\d+)?$", "", WELL_CONTENT)]

ggplot(CSBPE[!is.na(RAW2)], aes(x = TIMESTAMP, y = RAW2, color = WELL_ID)) +
  facet_wrap(~ SAMPLE_ID) +
  geom_point() +
  geom_line() +
  scale_color_viridis_d() +
  ggtitle("Clear Plates - Sample+Buffer (PEROX)") +
  xlab("Time of Reading") +
  ylab("Raw Reading (corrected)") +
  theme_bw() +
  theme(
    aspect.ratio = 1
  )

ggsave("fig/blanks/clear_sample+buffer_perox_check.png", width = 9, height = 6, units = "in")

