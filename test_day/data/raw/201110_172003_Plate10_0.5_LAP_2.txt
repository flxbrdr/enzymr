

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 10
Date	11/10/2020
Time	10:25:29 AM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.0

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	373	52	16	33	26	164	55	78	91	0	0	360/40,460/40
B	0	380	54	18	37	26	167	59	81	97	0	0	360/40,460/40
C	0	370	54	18	39	29	167	62	87	100	0	0	360/40,460/40
D	3	371	54	20	40	29	168	61	88	103	0	0	360/40,460/40
E	0	353	241	97	152	172	305	181	276	250	0	1	360/40,460/40
F	0	421	240	96	153	171	305	189	266	252	1	0	360/40,460/40
G	1	371	241	97	155	167	306	194	267	247	0	1	360/40,460/40
H	1	369	231	95	151	160	294	203	254	233	1	0	360/40,460/40

