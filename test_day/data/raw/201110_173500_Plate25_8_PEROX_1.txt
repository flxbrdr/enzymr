

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_PhenoxPerox_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_phenox_perox_450nm.prt



Plate Number	Plate 25
Date	11/10/2020
Time	5:33:56 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Absorbance Endpoint
	Full Plate
	Wavelengths:  450
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 8



Actual Temperature:	27.5

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0.043	0.117	0.051	1.169	1.402	1.476	1.369	1.858	1.531	2.351	3.244	3.131	450
B	0.042	0.051	0.047	1.167	1.650	1.506	1.380	1.830	2.110	2.348	3.131	3.196	450
C	0.040	0.049	0.052	1.151	1.439	1.564	1.413	1.868	1.652	2.304	3.218	3.115	450
D	0.045	0.049	0.068	1.044	1.680	1.525	1.315	1.937	1.867	2.365	3.245	3.128	450
E	0.041	0.044	0.044	1.123	1.597	1.585	1.404	1.943	2.025	2.405	3.188	3.166	450
F	0.037	0.044	0.043	1.093	1.514	1.500	1.349	1.703	1.864	2.376	3.247	3.188	450
G	0.039	0.046	0.044	1.121	1.480	1.475	1.224	1.903	1.769	2.369	3.183	3.173	450
H	0.044	0.048	0.046	0.987	1.296	1.365	1.349	1.766	1.963	2.330	3.062	3.032	450

