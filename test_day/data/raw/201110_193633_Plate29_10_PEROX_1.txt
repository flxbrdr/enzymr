

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_PhenoxPerox_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_phenox_perox_450nm.prt



Plate Number	Plate 29
Date	11/10/2020
Time	7:35:29 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Absorbance Endpoint
	Full Plate
	Wavelengths:  450
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 8



Actual Temperature:	27.3

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0.043	0.118	0.050	1.161	1.421	1.504	1.383	1.893	1.571	2.353	3.362	3.215	450
B	0.042	0.051	0.047	1.169	1.711	1.558	1.389	1.870	2.153	2.348	3.250	3.316	450
C	0.040	0.049	0.047	1.161	1.486	1.621	1.426	1.930	1.701	2.300	3.331	3.207	450
D	0.045	0.049	0.043	1.040	1.751	1.516	1.325	1.993	1.917	2.362	3.351	3.217	450
E	0.041	0.044	0.044	1.127	1.646	1.636	1.414	2.004	2.081	2.401	3.290	3.221	450
F	0.037	0.044	0.042	1.096	1.570	1.554	1.380	1.749	1.868	2.372	3.322	3.253	450
G	0.039	0.046	0.044	1.117	1.491	1.511	1.209	1.921	1.790	2.362	3.250	3.209	450
H	0.044	0.048	0.046	0.979	1.344	1.407	1.354	1.802	1.991	2.312	3.036	3.125	450

