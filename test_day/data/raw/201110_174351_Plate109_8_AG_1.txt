

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 109
Date	11/10/2020
Time	5:39:12 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.4

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	28	3	16	61	8	24	38	106	19	0	0	360/40,460/40
B	1	31	3	16	57	8	19	36	120	24	0	0	360/40,460/40
C	0	32	4	15	51	8	20	31	98	20	1	0	360/40,460/40
D	0	34	4	19	55	9	28	37	99	24	0	0	360/40,460/40
E	0	33	23	45	129	32	50	58	154	42	0	0	360/40,460/40
F	1	33	24	57	170	38	43	57	172	42	0	0	360/40,460/40
G	0	34	24	46	160	32	53	57	150	50	0	0	360/40,460/40
H	1	22	22	44	135	30	74	53	144	41	1	0	360/40,460/40

