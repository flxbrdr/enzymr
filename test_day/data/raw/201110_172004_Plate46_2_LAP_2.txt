

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 46
Date	11/10/2020
Time	11:51:15 AM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.4

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	364	48	36	39	31	150	102	81	95	0	0	360/40,460/40
B	0	376	52	36	42	33	157	105	86	104	0	0	360/40,460/40
C	0	365	52	37	44	36	159	105	90	109	1	1	360/40,460/40
D	1	369	53	38	46	35	161	106	93	111	0	0	360/40,460/40
E	0	351	231	153	159	187	296	316	279	257	1	2	360/40,460/40
F	1	417	231	153	162	188	292	322	272	256	1	0	360/40,460/40
G	1	368	228	157	159	181	290	331	272	247	0	1	360/40,460/40
H	1	364	213	155	152	156	270	344	245	228	1	0	360/40,460/40

