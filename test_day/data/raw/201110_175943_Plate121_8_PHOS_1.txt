

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 121
Date	11/10/2020
Time	5:58:39 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.5

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	32	36	445	566	268	142	750	1661	392	0	0	360/40,460/40
B	0	37	41	377	550	273	156	689	1595	400	0	0	360/40,460/40
C	0	40	41	370	457	267	137	660	1478	409	0	0	360/40,460/40
D	0	36	40	344	440	257	166	712	1458	384	0	0	360/40,460/40
E	0	36	154	798	1502	441	221	782	1795	437	0	0	360/40,460/40
F	0	38	155	806	1565	388	226	769	1927	419	1	0	360/40,460/40
G	1	36	147	817	1748	439	216	821	1999	462	0	0	360/40,460/40
H	1	34	139	824	1847	422	198	964	1945	496	0	0	360/40,460/40

