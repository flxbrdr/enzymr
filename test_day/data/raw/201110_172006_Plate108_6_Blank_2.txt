

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 108
Date	11/10/2020
Time	4:01:45 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.9

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	11	150	0	14	204	5	14	74	0	0	0	360/40,460/40
B	1	15	156	2	19	238	7	17	86	0	0	0	360/40,460/40
C	2	16	162	3	19	235	8	18	106	0	1	0	360/40,460/40
D	2	17	161	3	18	225	8	18	96	1	0	0	360/40,460/40
E	2	17	173	4	18	214	9	16	97	1	1	0	360/40,460/40
F	1	16	141	3	18	222	9	17	86	1	0	1	360/40,460/40
G	1	17	136	3	19	229	10	17	95	1	0	1	360/40,460/40
H	2	15	108	3	17	208	8	15	94	1	0	1	360/40,460/40

