

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 61
Date	11/10/2020
Time	12:50:18 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.9

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	32	1	10	67	5	13	17	119	8	0	0	360/40,460/40
B	0	32	3	11	65	6	15	18	98	9	0	0	360/40,460/40
C	0	36	2	13	54	7	16	21	114	10	0	1	360/40,460/40
D	1	36	4	10	48	5	12	19	92	10	1	0	360/40,460/40
E	1	34	16	27	87	15	31	32	109	24	1	0	360/40,460/40
F	1	35	16	25	92	15	31	32	140	24	1	0	360/40,460/40
G	1	35	16	25	143	15	31	36	131	23	1	1	360/40,460/40
H	1	34	15	24	92	15	27	34	102	23	1	1	360/40,460/40

