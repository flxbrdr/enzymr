

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 30
Date	11/10/2020
Time	11:02:57 AM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.1

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	22	0	5	14	54	9	11	18	122	0	0	360/40,460/40
B	1	22	2	6	14	51	9	12	26	91	0	0	360/40,460/40
C	0	24	2	7	16	52	10	13	23	89	0	0	360/40,460/40
D	1	26	3	7	15	51	10	14	24	92	0	0	360/40,460/40
E	4	24	25	22	34	100	33	29	33	81	0	0	360/40,460/40
F	1	26	24	23	33	104	34	28	36	92	1	1	360/40,460/40
G	1	26	25	21	32	101	34	27	33	90	0	0	360/40,460/40
H	1	26	25	23	31	130	32	28	33	128	0	1	360/40,460/40

