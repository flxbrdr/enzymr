

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 133
Date	11/10/2020
Time	7:51:36 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.2

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	33	0	71	374	32	28	90	759	27	0	0	360/40,460/40
B	0	32	2	69	338	27	35	87	624	36	0	0	360/40,460/40
C	0	38	2	77	309	37	36	115	902	41	0	0	360/40,460/40
D	0	38	3	64	309	32	22	112	605	33	0	0	360/40,460/40
E	0	35	16	146	691	47	33	96	644	44	0	0	360/40,460/40
F	0	36	15	111	635	47	34	92	771	46	0	0	360/40,460/40
G	1	35	15	102	896	43	33	99	735	40	0	1	360/40,460/40
H	1	32	14	94	531	34	25	101	546	40	1	1	360/40,460/40

