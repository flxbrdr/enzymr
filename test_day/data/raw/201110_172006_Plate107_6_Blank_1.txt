

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 107
Date	11/10/2020
Time	4:00:31 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.9

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	25	260	1	19	169	0	21	197	0	0	0	360/40,460/40
B	1	29	290	3	20	190	1	24	223	0	0	0	360/40,460/40
C	1	30	283	3	25	197	1	26	232	0	0	1	360/40,460/40
D	2	30	285	4	25	204	1	25	231	0	0	0	360/40,460/40
E	2	29	297	4	27	188	2	25	230	0	1	0	360/40,460/40
F	3	30	280	4	25	189	6	26	224	0	0	0	360/40,460/40
G	2	28	287	3	24	189	2	26	231	1	0	1	360/40,460/40
H	2	26	258	4	22	166	2	23	206	1	1	0	360/40,460/40

