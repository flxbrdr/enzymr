

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 58
Date	11/10/2020
Time	12:46:48 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.9

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	20	0	39	32	179	67	51	67	406	0	0	360/40,460/40
B	1	26	2	40	44	124	35	55	51	375	0	0	360/40,460/40
C	0	26	3	44	38	114	39	49	58	380	0	0	360/40,460/40
D	1	27	3	40	43	109	57	52	50	370	0	0	360/40,460/40
E	1	27	17	51	63	299	67	54	64	390	0	1	360/40,460/40
F	2	27	17	49	79	351	35	51	70	408	1	1	360/40,460/40
G	1	28	17	49	114	471	47	53	69	483	1	1	360/40,460/40
H	1	28	16	53	109	534	39	59	73	465	1	1	360/40,460/40

