

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 131
Date	11/10/2020
Time	7:48:42 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.2

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	16	0	31	96	17	24	65	430	35	0	0	360/40,460/40
B	0	27	2	44	145	20	21	79	283	33	0	0	360/40,460/40
C	0	28	2	40	150	21	32	81	350	38	1	0	360/40,460/40
D	0	28	2	44	151	23	19	79	300	39	0	0	360/40,460/40
E	0	29	11	95	410	115	22	100	398	64	1	0	360/40,460/40
F	0	28	13	89	370	60	27	104	395	62	1	1	360/40,460/40
G	1	29	12	97	438	64	26	109	426	68	0	0	360/40,460/40
H	0	25	9	86	372	50	26	120	403	84	0	0	360/40,460/40

