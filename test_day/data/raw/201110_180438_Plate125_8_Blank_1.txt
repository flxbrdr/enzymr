

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 125
Date	11/10/2020
Time	6:03:34 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.6

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	24	255	0	19	156	0	21	185	0	0	0	360/40,460/40
B	1	27	279	3	20	179	1	24	216	0	0	0	360/40,460/40
C	1	30	270	4	25	183	2	26	229	0	0	1	360/40,460/40
D	2	29	278	4	25	192	2	25	227	1	0	0	360/40,460/40
E	2	28	292	3	27	174	2	24	223	1	1	1	360/40,460/40
F	2	29	272	4	25	181	7	26	220	1	1	1	360/40,460/40
G	3	28	276	4	24	183	1	25	224	1	0	1	360/40,460/40
H	2	26	247	4	22	159	2	23	192	1	1	0	360/40,460/40

