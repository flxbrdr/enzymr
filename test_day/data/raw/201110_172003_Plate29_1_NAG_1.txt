

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 29
Date	11/10/2020
Time	11:01:33 AM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.1

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	28	0	19	40	4	9	46	91	15	0	0	360/40,460/40
B	0	27	2	20	35	5	10	33	70	12	0	0	360/40,460/40
C	0	32	3	19	37	6	12	41	71	12	0	0	360/40,460/40
D	0	31	2	21	38	6	12	34	76	15	0	0	360/40,460/40
E	0	32	29	50	84	26	35	49	74	29	0	0	360/40,460/40
F	0	33	29	49	105	27	38	49	78	33	0	0	360/40,460/40
G	1	32	29	50	91	26	36	50	74	31	0	0	360/40,460/40
H	0	32	28	56	105	27	34	55	88	33	0	0	360/40,460/40

