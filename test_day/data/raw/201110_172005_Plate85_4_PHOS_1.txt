

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 85
Date	11/10/2020
Time	2:00:50 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.1

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	32	32	241	411	138	114	401	980	211	0	0	360/40,460/40
B	0	36	35	221	385	125	119	351	954	201	0	0	360/40,460/40
C	0	40	35	203	279	122	110	326	852	196	0	0	360/40,460/40
D	0	36	35	184	264	122	124	348	821	189	0	0	360/40,460/40
E	0	36	145	397	780	220	188	416	1023	253	0	1	360/40,460/40
F	0	37	144	436	903	201	189	410	1131	244	0	1	360/40,460/40
G	0	37	141	456	1070	225	191	485	1178	265	0	1	360/40,460/40
H	1	36	138	458	1082	242	176	559	1138	283	1	0	360/40,460/40

