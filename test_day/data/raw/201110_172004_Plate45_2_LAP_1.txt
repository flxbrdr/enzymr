

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 45
Date	11/10/2020
Time	11:50:02 AM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.4

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	347	52	100	171	40	156	276	454	101	0	0	360/40,460/40
B	0	363	57	103	165	40	165	239	420	107	0	0	360/40,460/40
C	0	372	58	105	166	39	169	234	374	109	1	0	360/40,460/40
D	1	374	58	100	162	39	168	247	371	114	1	0	360/40,460/40
E	1	370	249	332	474	168	307	390	590	316	0	1	360/40,460/40
F	1	362	245	325	494	179	305	395	669	319	1	0	360/40,460/40
G	1	370	239	358	618	167	304	436	702	322	1	1	360/40,460/40
H	0	350	229	328	558	163	285	406	680	296	1	1	360/40,460/40

