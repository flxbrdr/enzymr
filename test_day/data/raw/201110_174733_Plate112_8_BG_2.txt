

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 112
Date	11/10/2020
Time	5:46:29 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.4

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	20	0	176	118	350	181	246	247	1021	0	0	360/40,460/40
B	0	26	2	181	151	337	97	279	238	1102	0	0	360/40,460/40
C	0	26	4	191	142	285	106	261	290	1158	0	0	360/40,460/40
D	1	26	3	181	161	277	161	285	246	1145	0	0	360/40,460/40
E	0	26	18	235	288	1180	149	235	271	1443	0	1	360/40,460/40
F	0	27	16	232	370	1390	54	218	318	1535	1	0	360/40,460/40
G	1	27	18	226	542	1504	96	222	252	1641	1	1	360/40,460/40
H	1	26	14	230	391	1502	68	229	231	1426	1	1	360/40,460/40

