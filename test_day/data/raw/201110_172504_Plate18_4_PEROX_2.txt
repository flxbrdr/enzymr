

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_PhenoxPerox_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_phenox_perox_450nm.prt



Plate Number	Plate 18
Date	11/10/2020
Time	1:36:58 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Absorbance Endpoint
	Full Plate
	Wavelengths:  450
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 8



Actual Temperature:	28.2

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0.045	0.037	0.040	3.041	3.367	3.512	2.079	2.492	2.524	2.028	2.385	2.415	450
B	0.043	0.036	0.045	3.027	3.467	3.428	2.259	2.512	2.500	2.510	2.821	2.536	450
C	0.041	0.037	0.045	3.053	3.493	3.487	2.246	2.500	2.520	3.123	2.710	2.757	450
D	0.055	0.036	0.043	2.992	3.495	3.540	2.229	2.497	2.511	2.580	2.806	2.587	450
E	0.040	0.037	0.049	3.084	3.626	3.437	2.232	2.530	2.554	2.188	2.853	2.561	450
F	0.045	0.037	0.068	3.050	3.679	3.490	2.242	2.550	2.580	2.122	2.646	2.316	450
G	0.043	0.037	0.044	3.034	3.725	3.592	2.232	2.561	2.599	2.334	2.641	2.376	450
H	0.057	0.038	0.040	2.940	3.645	3.509	1.944	2.551	2.498	1.925	2.235	2.747	450

