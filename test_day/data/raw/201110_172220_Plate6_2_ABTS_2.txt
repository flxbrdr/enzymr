

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_ABTS_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_ABTS_420nm.prt



Plate Number	Plate 6
Date	11/10/2020
Time	11:31:13 AM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Absorbance Endpoint
	Full Plate
	Wavelengths:  420
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 8



Actual Temperature:	27.4

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0.041	0.122	0.044	3.561	OVRFLW	3.786	2.300	2.332	2.342	2.538	OVRFLW	OVRFLW	420
B	0.036	0.114	0.043	3.264	3.392	3.824	2.276	2.383	2.393	2.623	OVRFLW	OVRFLW	420
C	0.039	0.115	0.043	3.313	3.753	3.770	2.464	2.430	2.357	2.837	OVRFLW	OVRFLW	420
D	0.039	0.115	0.045	3.309	3.644	3.814	2.238	2.353	2.353	2.729	OVRFLW	OVRFLW	420
E	0.036	0.116	0.039	3.256	3.963	3.830	2.270	2.386	2.320	2.696	OVRFLW	OVRFLW	420
F	0.038	0.135	0.041	3.321	OVRFLW	3.996	2.258	2.382	2.392	2.471	OVRFLW	OVRFLW	420
G	0.033	0.117	0.040	3.268	OVRFLW	OVRFLW	2.294	2.291	2.412	2.461	OVRFLW	OVRFLW	420
H	0.033	0.122	0.038	3.348	OVRFLW	OVRFLW	2.259	2.334	2.330	2.610	3.379	OVRFLW	420

