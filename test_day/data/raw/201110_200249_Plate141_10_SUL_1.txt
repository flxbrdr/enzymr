

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 141
Date	11/10/2020
Time	8:01:44 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.3

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	25	0	13	34	9	0	46	119	41	0	0	360/40,460/40
B	0	30	1	15	36	11	1	59	136	46	0	0	360/40,460/40
C	0	31	0	16	39	12	2	65	147	49	0	0	360/40,460/40
D	0	31	0	17	40	12	1	60	148	48	0	0	360/40,460/40
E	0	31	1	118	253	85	4	198	409	136	0	1	360/40,460/40
F	1	35	2	119	265	86	3	206	435	142	1	0	360/40,460/40
G	1	28	2	122	273	85	3	207	433	150	0	1	360/40,460/40
H	1	29	2	105	260	95	4	197	419	145	1	0	360/40,460/40

