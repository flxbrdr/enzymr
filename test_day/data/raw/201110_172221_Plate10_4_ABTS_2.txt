

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_ABTS_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_ABTS_420nm.prt



Plate Number	Plate 10
Date	11/10/2020
Time	1:33:57 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Absorbance Endpoint
	Full Plate
	Wavelengths:  420
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 8



Actual Temperature:	28.3

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0.041	0.123	0.043	3.615	OVRFLW	OVRFLW	2.295	2.416	2.415	2.535	OVRFLW	OVRFLW	420
B	0.034	0.115	0.043	3.276	3.634	OVRFLW	2.289	2.531	2.551	2.626	OVRFLW	OVRFLW	420
C	0.038	0.116	0.043	3.336	OVRFLW	OVRFLW	2.438	2.580	2.417	2.914	OVRFLW	OVRFLW	420
D	0.039	0.118	0.045	3.330	3.980	OVRFLW	2.241	2.534	2.483	2.791	OVRFLW	OVRFLW	420
E	0.036	0.119	0.039	3.283	OVRFLW	OVRFLW	2.266	2.563	2.460	2.677	OVRFLW	OVRFLW	420
F	0.039	0.138	0.042	3.330	OVRFLW	OVRFLW	2.265	2.503	2.528	2.434	OVRFLW	OVRFLW	420
G	0.034	0.119	0.040	3.300	OVRFLW	OVRFLW	2.294	2.442	2.644	2.474	OVRFLW	OVRFLW	420
H	0.033	0.123	0.039	3.348	OVRFLW	OVRFLW	2.269	2.435	2.472	2.647	OVRFLW	OVRFLW	420

