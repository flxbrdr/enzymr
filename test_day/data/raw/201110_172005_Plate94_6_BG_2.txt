

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-10_Enz_Test2\2020-11-10_Enz_Test2_BlackPlates_Felix.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 94
Date	11/10/2020
Time	3:43:26 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.8

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	21	0	122	84	319	135	157	168	845	0	0	360/40,460/40
B	0	26	2	119	107	283	69	173	151	891	0	0	360/40,460/40
C	0	26	4	129	98	227	76	156	180	934	0	0	360/40,460/40
D	0	26	3	120	114	220	115	173	152	916	0	0	360/40,460/40
E	1	26	16	149	182	844	112	146	168	1022	0	0	360/40,460/40
F	1	27	15	146	234	1097	44	138	201	1088	1	0	360/40,460/40
G	1	27	17	146	358	1258	74	142	180	1203	0	1	360/40,460/40
H	0	26	15	153	273	1209	56	158	162	1080	0	0	360/40,460/40

