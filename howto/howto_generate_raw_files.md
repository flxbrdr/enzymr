# Generate raw files with the Gen5 software
Follow these steps to automatically generate a raw file at each plate reading. This will save you a LOT of time (no painstaking copy-paste in spreadsheets). It is important to respect those file naming conventions as file name is part of the metadata used downstream in the R scripts (unless you want to modify the code!).

Author: Félix Brédoire (contact me on my uwyo email with any question)

_v. 2021-01-06_


## Create your own xpt files

1. Before performing any reading, create your xpt files with Gen5 by selecting the right protocols (one xpt file for black plates, one for abts, and/or one for phenox_perox).

2. For each file (i.e. each protocol), generate the number of plates you need (i.e. total number of readings to perform with the given protocol).

3. Name each plate with the convention corresponding to what you are doing today (i.e. [test_day](howto_test_day.md), [sample_day](howto_sample_day.md), or [sample_day_kinetics](howto_sample_day_kinetics.md))


## Program the autogeneration of raw files

For each of your xpt file (i.e. each protocol), follow these steps:

1. Click on the "Report/Export" icon.

![](fig/fig1.png)

2. Choose "New export to text...".

![](fig/fig2.png)

3. In "Properties", keep the default settings unless they differ from the following screenshot.

![](fig/fig3.png)

4. In "Content", keep the default settings unless they differ from the following screenshot.

![](fig/fig4.png)

5. In "Options/Workflow", select "Auto-execute on completion of the procedure (reading the plate)" and "Export Mode/Each plate in a separate file".

![](fig/fig5.png)

6. In "Options/File" File Naming Convention section, click on the play icon (triangle to the right) to generate the following file name template "\<DATE\>\_\<TIME\>\_\<PLATE\>\_\<PLATE_ID\>", note the use of underscore in between fields and the ".txt" extension. In the File Location section, choose where to save your raw reading files (avoid using my folder in this example!). Deselect any validation prompt.

![](fig/fig6.png)

7. In "Options/Format", keep the default settings unless they differ from the following screenshot.

![](fig/fig7.png)

8. Click "OK" and save the file. You are now ready to read plates!
