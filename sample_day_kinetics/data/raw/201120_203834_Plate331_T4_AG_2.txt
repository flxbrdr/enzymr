

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 331
Date	11/20/2020
Time	8:37:23 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.3

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	29	17	60	66	58	149	129	46	70	52	41	360/40,460/40
B	0	28	21	69	85	76	164	138	57	94	50	47	360/40,460/40
C	0	37	22	83	87	80	164	213	58	72	48	61	360/40,460/40
D	1	35	21	81	82	84	152	156	57	92	49	50	360/40,460/40
E	1	35	22	33	34	29	61	35	51	58	31	44	360/40,460/40
F	0	35	22	45	34	30	53	35	46	41	29	40	360/40,460/40
G	1	34	22	35	33	26	36	54	42	45	32	36	360/40,460/40
H	1	33	21	38	31	26	36	30	44	37	39	52	360/40,460/40

