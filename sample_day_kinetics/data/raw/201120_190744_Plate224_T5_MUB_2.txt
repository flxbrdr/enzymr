

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 224
Date	11/20/2020
Time	7:06:39 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.3

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	0	0	14	10	13	17	17	13	18	12	16	360/40,460/40
B	0	0	0	21	16	18	23	22	16	20	19	17	360/40,460/40
C	0	0	0	21	17	21	24	21	15	24	19	20	360/40,460/40
D	0	0	0	21	18	23	24	23	18	24	19	15	360/40,460/40
E	0	0	0	22	25	27	26	26	26	19	20	21	360/40,460/40
F	0	0	0	23	25	26	27	25	26	20	21	21	360/40,460/40
G	0	1	1	21	26	20	26	21	23	12	7	21	360/40,460/40
H	0	0	0	18	22	22	22	21	20	13	15	19	360/40,460/40

