

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Phenox_Perox.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_phenox_perox_450nm.prt



Plate Number	Plate 26
Date	11/20/2020
Time	7:52:23 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Absorbance Endpoint
	Full Plate
	Wavelengths:  450
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 8



Actual Temperature:	28.5

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0.037	0.038	0.038	0.200	0.093	0.094	0.065	0.094	0.102	0.276	0.108	0.091	450
B	0.037	0.039	0.037	0.116	0.122	0.099	0.182	0.331	0.109	0.306	0.096	0.102	450
C	0.038	0.041	0.040	0.152	0.095	0.091	0.068	0.127	0.095	0.309	0.096	0.113	450
D	0.038	0.040	0.038	0.169	0.085	0.102	0.077	0.101	0.104	0.279	0.092	0.112	450
E	0.036	0.039	0.039	0.157	0.102	0.086	0.124	0.104	0.109	0.322	0.189	0.141	450
F	0.037	0.036	0.038	0.219	0.118	0.100	0.132	0.128	0.125	0.379	0.183	0.130	450
G	0.037	0.039	0.038	0.157	0.213	0.093	0.136	0.166	0.102	0.299	0.194	0.145	450
H	0.039	0.040	0.046	0.163	0.112	0.086	0.197	0.220	0.104	0.058	0.182	0.151	450

