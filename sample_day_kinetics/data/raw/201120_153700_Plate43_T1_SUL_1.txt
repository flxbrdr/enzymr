

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 43
Date	11/20/2020
Time	3:35:40 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.6

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	30	0	28	18	52	30	19	22	27	64	35	360/40,460/40
B	0	34	0	31	21	50	22	25	24	29	66	39	360/40,460/40
C	0	35	1	32	21	55	22	29	25	31	57	41	360/40,460/40
D	0	33	2	31	20	49	22	44	24	32	59	46	360/40,460/40
E	0	31	1	29	10	56	24	31	14	43	43	32	360/40,460/40
F	1	38	2	24	11	48	22	23	9	43	54	27	360/40,460/40
G	0	37	2	27	11	47	25	20	11	45	52	26	360/40,460/40
H	1	29	2	26	9	47	25	21	11	40	41	29	360/40,460/40

