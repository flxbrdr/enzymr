

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 206
Date	11/20/2020
Time	4:45:16 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.6

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	0	0	15	12	15	19	17	15	19	14	17	360/40,460/40
B	0	0	0	22	18	20	24	23	18	22	21	19	360/40,460/40
C	0	0	0	23	18	22	25	22	17	25	20	20	360/40,460/40
D	1	0	0	22	19	23	26	24	21	25	21	16	360/40,460/40
E	1	1	1	23	26	27	26	25	26	19	20	22	360/40,460/40
F	0	1	0	23	26	27	27	25	27	20	21	22	360/40,460/40
G	1	1	0	21	26	21	26	23	25	13	8	22	360/40,460/40
H	0	1	1	19	22	22	24	22	22	13	16	20	360/40,460/40

