

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 63
Date	11/20/2020
Time	5:37:33 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.3

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	38	15	580	641	1328	1176	1422	1096	1358	1210	1590	360/40,460/40
B	0	44	21	529	685	1366	1353	1584	1113	1744	1464	1594	360/40,460/40
C	0	43	19	467	746	1277	1052	1402	1380	1584	1803	1276	360/40,460/40
D	1	39	19	556	641	1192	1178	1421	1112	1390	1433	1818	360/40,460/40
E	0	42	21	40	77	411	180	251	340	156	152	622	360/40,460/40
F	1	45	21	39	53	314	341	228	235	188	339	679	360/40,460/40
G	0	44	21	58	100	440	329	332	194	177	318	952	360/40,460/40
H	1	40	22	70	54	354	390	200	212	138	347	995	360/40,460/40

