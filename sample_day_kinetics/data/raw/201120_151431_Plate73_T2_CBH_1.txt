

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 73
Date	11/20/2020
Time	3:13:23 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.5

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	39	11	57	465	175	238	573	209	102	185	94	360/40,460/40
B	0	43	13	49	413	155	388	641	193	80	87	104	360/40,460/40
C	0	43	15	44	581	174	427	518	210	93	117	76	360/40,460/40
D	0	43	14	44	366	169	259	507	196	86	87	82	360/40,460/40
E	1	43	14	37	104	25	42	24	29	31	34	23	360/40,460/40
F	4	42	14	19	128	29	58	36	20	25	29	23	360/40,460/40
G	1	42	15	25	79	34	50	26	22	22	23	24	360/40,460/40
H	1	41	14	14	110	25	37	27	19	21	20	36	360/40,460/40

