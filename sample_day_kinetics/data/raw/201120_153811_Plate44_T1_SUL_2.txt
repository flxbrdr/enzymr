

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 44
Date	11/20/2020
Time	3:37:07 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.7

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	34	0	84	15	14	14	73	17	116	88	45	360/40,460/40
B	0	39	1	63	16	16	17	66	19	105	85	46	360/40,460/40
C	0	39	2	56	15	13	20	61	18	112	89	50	360/40,460/40
D	1	34	2	57	15	15	17	58	17	102	85	54	360/40,460/40
E	0	38	2	57	32	38	20	70	55	54	97	52	360/40,460/40
F	0	34	3	55	25	47	25	77	45	52	91	51	360/40,460/40
G	1	34	2	58	19	36	16	69	43	47	82	43	360/40,460/40
H	0	30	2	52	17	38	14	61	38	49	75	46	360/40,460/40

