

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 26
Date	11/20/2020
Time	11:14:21 AM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.3

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	364	282	230	230	215	255	250	229	236	226	227	360/40,460/40
B	0	375	276	259	254	268	275	264	251	250	244	240	360/40,460/40
C	0	378	288	267	258	262	273	255	258	250	254	239	360/40,460/40
D	0	387	282	264	253	269	269	268	238	252	256	240	360/40,460/40
E	1	373	285	244	249	272	241	249	253	222	224	222	360/40,460/40
F	1	359	276	237	259	270	250	263	246	228	227	220	360/40,460/40
G	1	358	283	236	247	267	234	261	234	218	218	216	360/40,460/40
H	1	367	269	219	231	239	211	229	227	213	206	208	360/40,460/40

