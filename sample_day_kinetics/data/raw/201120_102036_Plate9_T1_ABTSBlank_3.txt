

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_ABTS.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_ABTS_420nm.prt



Plate Number	Plate 9
Date	11/20/2020
Time	10:19:33 AM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Absorbance Endpoint
	Full Plate
	Wavelengths:  420
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 8



Actual Temperature:	26.3

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0.037	0.035	0.038	0.217	0.247	0.133	0.100	0.119	0.150	0.114	0.089	0.104	420
B	0.035	0.036	0.037	0.182	0.227	0.113	0.091	0.103	0.136	0.115	0.090	0.104	420
C	0.035	0.048	0.034	0.188	0.236	0.112	0.087	0.098	0.136	0.123	0.090	0.102	420
D	0.034	0.035	0.035	0.189	0.233	0.115	0.083	0.099	0.141	0.128	0.090	0.109	420
E	0.042	0.035	0.037	0.238	0.223	0.168	0.217	0.176	0.195	0.148	0.213	0.129	420
F	0.036	0.035	0.035	0.232	0.215	0.162	0.212	0.170	0.190	0.141	0.214	0.124	420
G	0.035	0.036	0.036	0.226	0.215	0.149	0.201	0.163	0.182	0.139	0.199	0.123	420
H	0.053	0.035	0.035	0.243	0.241	0.132	0.204	0.147	0.177	0.131	0.199	0.133	420

