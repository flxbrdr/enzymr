

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Phenox_Perox.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_phenox_perox_450nm.prt



Plate Number	Plate 51
Date	11/20/2020
Time	11:10:31 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Absorbance Endpoint
	Full Plate
	Wavelengths:  450
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 8



Actual Temperature:	28.2

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0.042	0.036	0.037	0.296	0.299	0.172	0.147	0.181	0.223	0.229	0.147	0.186	450
B	0.034	0.034	0.034	0.270	0.290	0.166	0.147	0.181	0.226	0.266	0.165	0.192	450
C	0.034	0.039	0.033	0.259	0.278	0.152	0.129	0.163	0.217	0.201	0.151	0.164	450
D	0.034	0.035	0.033	0.247	0.283	0.159	0.138	0.142	0.208	0.218	0.164	0.170	450
E	0.035	0.035	0.034	0.283	0.260	0.242	0.300	0.251	0.290	0.207	0.339	0.203	450
F	0.035	0.037	0.034	0.295	0.268	0.246	0.316	0.238	0.240	0.223	0.322	0.218	450
G	0.035	0.038	0.035	0.314	0.289	0.233	0.325	0.254	0.314	0.231	0.319	0.212	450
H	0.039	0.036	0.037	0.392	0.307	0.245	0.355	0.308	0.339	0.229	0.337	0.229	450

