

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 38
Date	11/20/2020
Time	11:26:37 AM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	27.5

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	30	248	285	273	231	285	299	249	246	235	227	360/40,460/40
B	0	37	259	309	293	260	313	305	264	254	231	237	360/40,460/40
C	0	35	263	281	288	242	301	296	248	245	229	243	360/40,460/40
D	1	33	255	321	291	254	296	326	266	259	230	254	360/40,460/40
E	0	33	251	194	207	212	193	224	194	158	177	189	360/40,460/40
F	0	32	261	204	204	213	198	219	202	157	174	188	360/40,460/40
G	1	31	255	195	208	210	201	217	201	166	170	186	360/40,460/40
H	1	28	236	202	201	209	202	217	197	163	156	191	360/40,460/40

