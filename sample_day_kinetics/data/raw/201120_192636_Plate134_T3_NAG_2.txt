

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 134
Date	11/20/2020
Time	7:25:31 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.4

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	30	13	1241	1102	253	1437	703	688	552	711	711	360/40,460/40
B	0	38	18	1065	1029	230	1461	947	770	612	820	618	360/40,460/40
C	0	37	17	1269	1073	241	1463	804	689	722	814	671	360/40,460/40
D	0	35	18	1088	961	229	1232	648	654	639	782	805	360/40,460/40
E	0	33	19	259	257	82	501	101	207	190	356	184	360/40,460/40
F	1	35	20	392	306	135	393	97	259	362	362	138	360/40,460/40
G	0	31	19	256	466	81	378	80	196	206	308	141	360/40,460/40
H	1	31	18	252	420	71	458	73	196	171	412	129	360/40,460/40

