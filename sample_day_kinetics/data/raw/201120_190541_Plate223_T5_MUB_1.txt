

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 223
Date	11/20/2020
Time	7:04:36 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.3

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	0	0	13	21	16	19	26	17	31	18	22	360/40,460/40
B	0	0	0	19	23	23	22	27	22	35	22	24	360/40,460/40
C	0	0	0	18	20	25	24	30	22	39	23	26	360/40,460/40
D	0	0	0	20	20	24	24	30	21	37	20	26	360/40,460/40
E	0	0	0	23	27	29	29	28	40	24	24	21	360/40,460/40
F	0	0	0	19	27	28	31	28	25	26	26	21	360/40,460/40
G	1	0	0	6	1	29	31	28	25	24	25	21	360/40,460/40
H	1	0	0	11	2	26	24	24	23	23	23	19	360/40,460/40

