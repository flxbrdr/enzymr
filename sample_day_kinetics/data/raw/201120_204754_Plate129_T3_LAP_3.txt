

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 129
Date	11/20/2020
Time	8:46:48 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.3

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	332	234	685	635	573	732	894	609	729	1069	471	360/40,460/40
B	0	365	263	563	676	615	968	1103	624	987	1191	655	360/40,460/40
C	0	349	263	580	662	627	919	1055	559	833	1163	596	360/40,460/40
D	1	345	262	585	714	635	1011	880	507	1625	1486	580	360/40,460/40
E	0	367	263	289	445	373	517	417	408	519	572	480	360/40,460/40
F	0	362	258	303	453	392	577	393	413	611	545	428	360/40,460/40
G	1	341	250	269	429	362	546	351	418	542	538	458	360/40,460/40
H	0	274	209	251	437	321	557	402	360	456	487	437	360/40,460/40

