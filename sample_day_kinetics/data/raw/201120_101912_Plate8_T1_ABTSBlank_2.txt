

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_ABTS.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_ABTS_420nm.prt



Plate Number	Plate 8
Date	11/20/2020
Time	10:17:44 AM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Absorbance Endpoint
	Full Plate
	Wavelengths:  420
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 8



Actual Temperature:	26.3

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0.039	0.039	0.040	0.299	0.165	0.163	0.117	0.122	0.171	0.167	0.203	0.137	420
B	0.039	0.040	0.039	0.246	0.144	0.140	0.104	0.103	0.169	0.152	0.174	0.131	420
C	0.041	0.043	0.042	0.178	0.150	0.138	0.087	0.095	0.153	0.146	0.187	0.131	420
D	0.040	0.043	0.040	0.185	0.148	0.139	0.101	0.094	0.157	0.143	0.172	0.135	420
E	0.038	0.041	0.041	0.350	0.202	0.181	0.261	0.185	0.221	0.427	0.354	0.243	420
F	0.039	0.037	0.040	0.402	0.203	0.195	0.277	0.205	0.225	0.462	0.364	0.238	420
G	0.038	0.041	0.040	0.328	0.205	0.183	0.307	0.191	0.206	0.462	0.374	0.231	420
H	0.046	0.042	0.049	0.323	0.217	0.184	0.289	0.191	0.205	0.464	0.346	0.237	420

