

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 163
Date	11/20/2020
Time	4:35:40 PM
Reader Type:	Synergy HTX
Reader Serial Number:	Unknown
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.8

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	0	0	5	3	2	6	12	5	19	4	13	360/40,460/40
B	1	0	0	5	4	2	8	14	5	20	11	14	360/40,460/40
C	0	1	0	6	4	3	8	13	5	20	4	14	360/40,460/40
D	1	1	1	5	5	2	8	14	5	20	5	15	360/40,460/40
E	1	1	1	3	1	2	3	3	2	4	2	2	360/40,460/40
F	0	1	1	2	2	1	4	2	2	4	2	3	360/40,460/40
G	1	1	1	3	2	1	3	2	2	4	2	2	360/40,460/40
H	1	1	1	3	2	1	3	3	2	3	3	2	360/40,460/40

