

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Phenox_Perox.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_phenox_perox_450nm.prt



Plate Number	Plate 50
Date	11/20/2020
Time	11:09:14 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Absorbance Endpoint
	Full Plate
	Wavelengths:  450
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 8



Actual Temperature:	28.3

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0.044	0.041	0.035	0.264	0.215	0.187	0.187	0.198	0.244	0.202	0.231	0.206	450
B	0.036	0.038	0.034	0.240	0.214	0.179	0.168	0.186	0.217	0.215	0.225	0.188	450
C	0.036	0.044	0.040	0.238	0.190	0.172	0.162	0.174	0.204	0.206	0.212	0.176	450
D	0.036	0.038	0.035	0.230	0.194	0.166	0.164	0.167	0.215	0.199	0.227	0.185	450
E	0.037	0.038	0.034	0.392	0.261	0.215	0.338	0.217	0.245	0.481	0.374	0.274	450
F	0.036	0.039	0.034	0.368	0.291	0.212	0.340	0.221	0.248	0.483	0.366	0.294	450
G	0.040	0.043	0.034	0.412	0.299	0.218	0.361	0.238	0.249	0.472	0.366	0.271	450
H	0.036	0.038	0.035	0.388	0.334	0.251	0.374	0.238	0.257	0.456	0.373	0.285	450

