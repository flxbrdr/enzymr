

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 158
Date	11/20/2020
Time	6:55:23 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.4

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	31	19	156	416	208	314	284	182	130	128	100	360/40,460/40
B	0	37	23	175	402	221	308	343	195	134	128	110	360/40,460/40
C	0	38	24	177	406	210	285	324	174	118	120	115	360/40,460/40
D	1	37	24	196	401	210	203	295	166	148	118	111	360/40,460/40
E	0	39	24	55	58	56	122	100	66	72	53	81	360/40,460/40
F	1	36	24	57	84	54	88	60	59	56	49	89	360/40,460/40
G	0	36	24	54	83	53	88	64	53	69	44	59	360/40,460/40
H	1	34	22	53	89	58	90	61	60	48	45	55	360/40,460/40

