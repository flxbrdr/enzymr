

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 201
Date	11/20/2020
Time	2:36:55 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.1

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	0	0	146	169	85	119	115	105	160	135	75	360/40,460/40
B	0	0	0	176	193	125	154	146	147	214	140	65	360/40,460/40
C	0	0	0	183	184	128	152	156	143	229	126	103	360/40,460/40
D	0	0	0	191	198	143	155	169	152	207	118	94	360/40,460/40
E	0	0	0	265	258	284	258	264	277	278	286	273	360/40,460/40
F	1	1	0	264	259	277	255	264	270	248	278	262	360/40,460/40
G	1	1	1	259	244	284	225	262	274	264	275	245	360/40,460/40
H	1	1	1	237	238	245	196	235	245	232	245	223	360/40,460/40

