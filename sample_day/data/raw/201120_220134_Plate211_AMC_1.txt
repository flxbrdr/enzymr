

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 211
Date	11/20/2020
Time	10:00:29 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.5

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	0	0	70	73	139	130	94	99	165	110	74	360/40,460/40
B	0	0	0	91	117	186	160	117	100	193	120	92	360/40,460/40
C	0	0	0	87	137	174	148	128	114	186	126	90	360/40,460/40
D	0	0	0	77	129	191	138	132	109	177	125	117	360/40,460/40
E	0	0	0	241	290	288	256	189	260	192	237	190	360/40,460/40
F	0	1	0	255	269	284	267	253	256	229	264	200	360/40,460/40
G	0	0	0	233	236	278	257	246	254	236	256	182	360/40,460/40
H	0	0	0	137	243	226	102	229	223	215	213	188	360/40,460/40

