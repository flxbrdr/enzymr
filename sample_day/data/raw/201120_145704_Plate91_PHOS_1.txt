

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 91
Date	11/20/2020
Time	2:55:57 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.3

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	31	249	1545	1370	874	1545	1531	1376	2139	1436	1827	360/40,460/40
B	0	35	259	1255	1276	783	1490	1354	1336	1781	1395	1745	360/40,460/40
C	0	37	263	1244	1169	788	1261	1282	1295	1688	1465	1740	360/40,460/40
D	0	36	260	1215	1010	833	1346	1316	1235	1658	1457	1817	360/40,460/40
E	0	34	245	342	295	375	404	438	330	606	329	306	360/40,460/40
F	1	35	266	299	296	362	433	367	319	494	362	309	360/40,460/40
G	0	33	254	322	308	359	499	384	322	517	330	302	360/40,460/40
H	0	31	240	311	296	356	372	367	337	457	322	312	360/40,460/40

