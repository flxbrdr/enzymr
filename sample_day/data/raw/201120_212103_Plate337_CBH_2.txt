

Software Version	2.09.1



Experiment File Path:	C:\Users\Public\Documents\Felix_Enzymes\2020-11-20_Enz2\2020-11-20_Enz2_Black_Plates.xpt
Protocol File Path:	C:\Users\Public\Documents\Protocols\Enzymes18_blackplates_360_340.prt



Plate Number	Plate 337
Date	11/20/2020
Time	9:19:58 PM
Reader Type:	Synergy HTX
Reader Serial Number:	15121412
Reading Type	Reader

Procedure Details

Plate Type	96 WELL PLATE
Eject plate on completion	
Read	Fluorescence Endpoint
	Full Plate
	Filter Set 1
	    Excitation: 360/40,  Emission: 460/40
	    Optics: Top,  Gain: 35
	Light Source: Tungsten,  Standard Dynamic Range
	Read Speed: Normal,  Delay: 100 msec,  Measurements/Data Point: 10
	Read Height: 1 mm



Actual Temperature:	28.3

Results
	1	2	3	4	5	6	7	8	9	10	11	12
A	0	30	9	333	927	497	687	934	173	121	221	105	360/40,460/40
B	0	37	18	299	1104	671	1117	978	233	158	289	115	360/40,460/40
C	0	37	15	407	1088	651	906	994	252	189	266	105	360/40,460/40
D	0	36	16	316	1097	684	864	1188	231	214	231	115	360/40,460/40
E	0	37	16	55	165	94	214	106	81	75	35	133	360/40,460/40
F	0	37	16	38	137	87	148	328	76	75	40	137	360/40,460/40
G	0	36	17	63	132	260	201	86	51	114	37	176	360/40,460/40
H	0	34	16	90	118	77	121	69	78	83	35	108	360/40,460/40

